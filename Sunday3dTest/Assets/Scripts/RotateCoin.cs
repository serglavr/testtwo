using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCoin : MonoBehaviour
{
    [SerializeField] private GameObject Coin;
    // Start is called before the first frame update
    
    public float rotationSpeed = 100f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Coin != null)
        {
            var transform = Coin.GetComponent<Transform>();

            var x = transform.rotation.x;
            var z = transform.rotation.z;
            var w = transform.rotation.w;
            
            if (transform.rotation.y == 359)
            {
                transform.rotation.Set(x,0,z,w);
            }
            
            transform.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime);
        }
    }
}
