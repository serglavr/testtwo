using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeColor : MonoBehaviour
{
    [SerializeField] public Button ChangeColorButton;

    [SerializeField] public Renderer Coin;
    // Start is called before the first frame update
    void Start()
    {
        ChangeColorButton.onClick.AddListener(() => ChangeCoinColor());
    }

    private void ChangeCoinColor()
    {
        var r = Random.Range(0f, 1f);
        var g = Random.Range(0f, 1f);
        var b = Random.Range(0f, 1f);

        Coin.material.color = new Color(r, g, b,1f);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
