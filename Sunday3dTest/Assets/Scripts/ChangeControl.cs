using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeControl : MonoBehaviour
{
    private static int _activeModel = 1;

    [SerializeField] private Button ChangeModelButton;
    
    [SerializeField] public Camera CarCamera;
    [SerializeField] public GameObject CarControl;
    
    [SerializeField] public Camera CoinCamera;
    [SerializeField] public GameObject CoinControl;

    // Start is called before the first frame update
    void Start()
    {
        EnableCamera(_activeModel);
        ChangeModelButton.onClick.AddListener(() => ChangeModel(_activeModel));
    }

    private void ChangeModel(int model)
    {
        switch (model)
        {
            case 1:
                _activeModel = 2;
                EnableCamera(_activeModel);
                break;
            case 2:
                _activeModel = 1;
                EnableCamera(_activeModel);
                break;
        }
    }

    private void EnableCamera(int model)
    {
        switch (model)
        {
            case 1:
                EnableNeededThings(true, false);
                break;
            case 2:
                EnableNeededThings(false, true);
                break;
        }
    }

    private void EnableNeededThings(bool car, bool coin)
    {
        CarCamera.enabled = car;
        CarControl.gameObject.SetActive(car);
        
        CoinCamera.enabled = coin;
        CoinControl.gameObject.SetActive(coin);
    }
}
